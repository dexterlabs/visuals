# Visuals

Visual coding playgrounds

# Setup (sapper)
- All visuals are indexed from a single file in a dir (not needing to import)
- Dynamically import
- SSR Rendering a still image for every visual (configurable)

## Ideas
- [ ] Fractally shapes
  - [ ] Zooming shapes
  - [ ] Growing shapes
- [ ] Waves

## Todo's
- [ ] Livecoding hot reloading REPL
- [ ] Visuals as components
- [ ] Modify existing -> Save to library
- [ ] Find a nice noise library
  - [x] https://www.npmjs.com/package/tooloud


- Better animation using css/svelte animation primitives
- Nice edit flows, quickly saving edits [REPL]
- Auto index generation (save in browser could trigger download -> library dir -> generate all.js)

If I fix the hot-reloader in the repl (first fix the repl) I might have a nice grasp and can direct how errors should behave
