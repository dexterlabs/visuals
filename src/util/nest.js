import { array } from 'src/util'

function boundedIntersection([[x1, y1], [x2, y2]], [[x3, y3], [x4, y4]]) {
  const t = (
      ((x1 - x3)*(y3 - y4) - (y1 - y3)*(x3 - x4))
    / ((x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4))
  )
  const u = (
      - ((x1 - x2)*(y1 - y3) - (y1 - y2)*(x1 - x3))
    / ((x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4))
  )
  return [
    (t >= 0) && (t <= 1) && [x1 + t*(x2 - x1), y1 + t*(y2 - y1)],
    (u >= 0) && (u <= 1) && [x3 + u*(x4 - x3), y3 + u*(y4 - y3)],
  ]
}
export function nest(lines, {
  sections = 3,
  origin = [0, 0],
  rotation = 0,
} = {}) {
  return array(sections)
    .map(value => ([
      origin,
      [
        Math.sin(rotation + (value * Math.PI * 2/sections)) * 10000000000,
        Math.cos(rotation + (value * Math.PI * 2/sections)) * 10000000000
      ]
    ]))
    .map(intersector => lines.reduce(
      (acc, p, i, arr) => {
        const [t1, t2] = boundedIntersection(
          [arr[i], arr[(i + 1) % arr.length]],
          intersector
        )
        if (t1 && t2) {
          return t1
        }
        return acc
      }, null)
    ).filter(i => i)
}  