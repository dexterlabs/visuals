/* accepts parameters
 * h  Object = {h:x, s:y, v:z}
 * OR 
 * h, s, v
*/
export default function HSVtoRGB(_h=0, s=1, v=1, a=1) {
  let r, g, b
  const h = _h / (2*Math.PI)
  const i = Math.floor(h * 6);
  const f = h * 6 - i;
  const p = v * (1 - s);
  const q = v * (1 - f * s);
  const t = v * (1 - (1 - f) * s);
  switch (i % 6) {
      case 0: r = v, g = t, b = p; break;
      case 1: r = q, g = v, b = p; break;
      case 2: r = p, g = v, b = t; break;
      case 3: r = p, g = q, b = v; break;
      case 4: r = t, g = p, b = v; break;
      case 5: r = v, g = p, b = q; break;
  }
  return `rgba(${
    Math.round(r * 255)
  },${
    Math.round(g * 255)
  },${
    Math.round(b * 255)
  }, ${a})`
}