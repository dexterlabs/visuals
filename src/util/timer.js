import { readable, derived } from 'svelte/store'

// Store to update 'time' variable
// Stage time into multiple setups


export const time = readable(new Date().getTime(), set => {
  let animationFrame
  const tick = () => {
    set(new Date().getTime())
    animationFrame = requestAnimationFrame(tick)
  }
  if (process.browser) {
    tick()
    return () => cancelAnimationFrame(animationFrame)
  }
})

export const divide = (time, amount) => derived(time, $time => $time / amount)

// when time != 0
export const when = (time) => derived(time, $time => $time !== 0)

/* Split time into multiple parts
 * returns an array of stores, each being updated with a value between 0...1 
*/
export function staged(time, ...parts) {

  let start
  const total = parts.reduce((sum, value) => sum + value, 0)
  const setters = new Array(parts.length)
  let normal = true

  const splitter = time => {
    if (!start) {
      // Initialize
      start = time
    }
    while (start + total < time) {
      // Reset
      start += total
      normal = !normal
    }
    let current = normal ? time - start : total - (time - start)

    parts.forEach((duration, index) => {
      const setter = setters[index]

      if (setter) {
        const v = current / duration
        const value = v > 1 ? 1 : (v < 0 ? 0 : v)
        setter(value)
      }
      current -= duration
    })
  }
  let unsubscribeTime

  return parts.map((part, index) => 
    readable(0, set => {
      // Subscribe time
      if (!unsubscribeTime) {
        unsubscribeTime = time.subscribe(splitter)
      }
      // register setter
      setters[index] = set

      return () => {
        if (setters.filter(v => v).length === 0) {
          unsubscribeTime()
          unsubscribeTime = null
        }
        setters[index] = null
        
      }
    })
  )
}

export const interval = interval => readable(new Date().getTime(), set => {
  const id = setInterval(() => set(new Date().getTime()), interval);

  return () => clearInterval(id)
})