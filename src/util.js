

// timre tries to call in frequency
export const timer = (frequency, callback) => {
  let running = false
  let time = new Date().getTime()
  let timeout = 1000 / frequency

  const tick = () => {
    if (!running) return

    const now = new Date().getTime()
    const diff = now - time
    time = now
    callback(time)

    if (diff > (timeout * 2)) {
      requestAnimationFrame(tick)
    } else {
      setTimeout(tick, timeout)
    }
  }

  return {
    stop: () => { running = false },
    start: () => {
      running = true
      tick()
    }
  }
}

export const pulse = (t, div=100) => ((Math.floor(t / div) % 2) ? (div - (t % div)) : (t % div)) / div

export const array = (n) => ([...new Array(n).keys()])
export const range = (a, b, step = 1) => {
  // Range from a - b, or 0 - a
  if (!b) {
    b = a
    a = 0
  }
  return array(Math.round((b-a)/step)).map(i => a + (step * i))
}

export const sin = v => (Math.sin(v) + 1) / 2
export const cos = v => (Math.cos(v) + 1) / 2

export const dot = (v1, v2) => {
  return v1.map((v, i) => v * v2[i])
    .reduce((acc, val) => acc + val, 0)
}
export const abs = ([x, y]) => {
  return Math.sqrt(x*x + y*y)
}
export const angle = (v1, v2) => {
  return dot(v1, v2) / (abs(v1) * abs(v2))
}
export const diff = (base, v) => v.map((v, i) => base[i] - v)

export const project = ([x1, y1], [x2, y2], [px, py]) => {
  const m = (y2 - y1) / (x2 - x1)
  const b = y1 - (m * x1)
  const x = (m * py + px - m * b) / (m * m + 1)
  const y = (m * m * py + m * px + b) / (m * m + 1)

  return [x, y]
}

export const rgba = (r=255,g=255,b=255,a=1) => `rgba(${[r,g,b,a]})`


// Svg Path commands

function params(n, params) {
  if (n > 1) {
    return params.map(arg => arg.join(',')).join(' ')
  } else {
    return params.join(' ')
  }
}
function command(letter, nParams) {
  return (...args) => `${letter}${params(nParams, args)}`
}

export const svg = {

  d: (...commands) => commands.join('\n'),

  // MoveTo
  M: command('M', 2), // (x, y)+
  m: command('m', 2), // (dx, dy)+

  // LineTo
  L: command('L', 2), // (x, y)+
  l: command('l', 2), // (dx, dy)+
  H: command('H', 1), // x+
  h: command('h', 1), // dx+
  V: command('V', 1), // y+
  v: command('v', 1), // dy+

  // Cubic Bezier Curve
  C: command('C', 6), // (x1, y1, x2, y2, x, y)+
  c: command('c', 6), // (dx1, dy1, dx2, dy2, dx, dy)+
  S: command('S', 4), // (x2, y2, x, y)+
  s: command('s', 4), // (dx2, dy2, dx, dy)+

  // Quadratic Bezier Curve
  Q: command('Q', 4), // (x1, y1, x, y)+
  q: command('q', 4), // (dx1, dy1, dx, dy)+
  T: command('T', 2), // (x, y)+
  t: command('t', 2), // (dx, dy)+

  // Elliptical Arc Curve
  A: command('A', 7),
  a: command('a', 7),

  // ClosePath
  Z: command('Z', 0),
  z: command('z', 0),
}



export const blob = (points, radius) => {
  if (!points) return

  const p = array(points)
    .map(i => ([
      Math.cos((i / points) * 2 * Math.PI) * radius,
      Math.sin((i / points) * 2 * Math.PI) * radius
    ]))
  const q = [...p.slice(1), p[0]].reduce((acc, cur, i) => {
    const from = p[i]
    return [...acc, [from[0], cur[1], ...cur]]
  }, [])

  return d(
    M(p[0]),
    Q(...q),
    Z()
  )
}


export const circle = (points, radius) => {
  if (!points) return

  const p = array(points)
    .map(i => ([
      Math.cos((i / points) * 2 * Math.PI) * radius,
      Math.sin((i / points) * 2 * Math.PI) * radius
    ]))

  return d(
    M(p[0]),
    L(...p),
    Z()
  )
}


export const star = (points, step=2) => array(Math.floor(points))
  .map(p => (p * step) % points)
  .map(p => [
    Math.sin(p * 2 * Math.PI / points) * size/2,
    Math.cos(p * 2 * Math.PI / points) * size/2,
  ])
  .join(' ')
  

  const spiro = ({
    points = 5000,
    size = 30,
    ratio = 1.2,
    ratio2 = 1.2,
    size2 = 28,
    resolution = 80,
  } = {}) => array(points)
    .map(i => ([
      Math.sin(i/resolution * Math.PI) * size
      + Math.sin(i/resolution * Math.PI * ratio) * size2
      + Math.sin(i/resolution * Math.PI * ratio2) * size2,
      Math.cos(i/resolution * Math.PI) * size
      + Math.cos(i/resolution * Math.PI * ratio) * size2
      + Math.cos(i/resolution * Math.PI * ratio2) * size2,
    ]))
  