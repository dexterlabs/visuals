import spirals from './15-11-2019.spirals.svelte'
import pulsy from './16-11-2019.pulsy.svelte'
import pulsy2 from './16-11-2019.pulsy2.svelte'
import spinny from './16-11-2019.spinny.svelte'
import statebox from './17-11-2019.statebox.svelte'
import statebox2 from './17-11-2019.statebox2.svelte'
import spiralbox from './17-11-2019.spiralbox.svelte'
import spiralbox2 from './17-11-2019.spiralbox2.svelte'
import pattern from './18-11-2019.pattern.svelte'
import spiralcircles from './19-11-2019.spiralcircles.svelte'
import spiralcircles2 from './19-11-2019.spiralcircles2.svelte'
import spiralcircles3 from './19-11-2019.spiralcircles3.svelte'
import spiralpulse from './19-11-2019.spiralpulse.svelte'
import spiralwave from './19-11-2019.spiralwave.svelte'

import colorfulSpiral from './25-11-2019.colorful-spiral.svelte'
import connected from './25-11-2019.connected.svelte'
import fastSpiral from './25-11-2019.fast-spiral.svelte'

// import growingTrees1 from './26-11-2019.growing-trees-try-1.svelte'
import growingTrees2 from './26-11-2019.growing-trees-try-2.svelte'

import squares2 from './05-12-2019.squares2.svelte'
import squares3 from './05-12-2019.squares3.svelte'
import circles from './09-12-2019.circles.svelte'
import mirrors from './13-12-2019.mirrors.svelte'
import mirrors2 from './16-12-2019.mirrors2.svelte'

import arcCircles1 from './20-12-2019.arc-circles1.svelte'
import arcCircles2 from './21-12-2019.arc-circles2.svelte'
import arcCircles3 from './21-12-2019.arc-circles3.svelte'

import spiro1 from './27-12-2019.spirograph-fast1.svelte'
import spiro2 from './27-12-2019.spirograph-fast2.svelte'
import spiro3 from './27-12-2019.spirograph-fast3.svelte'
import spiro4 from './27-12-2019.spirograph-fast4.svelte'
import spiroPretty from './27-12-2019.spirograph-pretty.svelte'
import spiro5 from './27-12-2019.spirograph5.svelte'

import nesting1 from './28-12-2019.nesting1.svelte'
import statebox3 from './28-12-2019.statebox3.svelte'

import zoomy from './31-12-2019.zoomy.svelte'
import tunnelvision from './31-12-2019.tunnelvision.svelte'

export default {
  '15-11-2019.spirals': spirals,
  '16-11-2019.pulsy': pulsy,
  '16-11-2019.pulsy2': pulsy2,
  '16-11-2019.spinny': spinny,
  '17-11-2019.statebox': statebox,
  '17-11-2019.statebox2': statebox2,
  '17-11-2019.spiralbox': spiralbox,
  '17-11-2019.spiralbox2': spiralbox2,
  '18-11-2019.pattern': pattern,
  // '19-11-2019.spiralcircles': spiralcircles,
  '19-11-2019.spiralcircles2': spiralcircles2,
  // '19-11-2019.spiralcircles3': spiralcircles3,
  '19-11-2019.spiralpulse': spiralpulse,
  // '19-11-2019.spiralwave': spiralwave,

  '25-11-2019.colorful-spiral': colorfulSpiral,
  '25-11-2019.connected': connected,
  '25-11-2019.fast-spiral': fastSpiral,

  // './26-11-2019.growing-trees-try-1': growingTrees1,
  '26-11-2019.growing-trees-try-2': growingTrees2,

  '05-12-2019.squares2': squares2,
  '05-12-2019.squares3': squares3,
  '09-12-2019.circles': circles,

  '13-12-2019.mirrors': mirrors,
  '16-12-2019.mirrors': mirrors2,
  '20-12-2019.arc-circles1': arcCircles1,
  '21-12-2019.arc-circles2': arcCircles2,
  '21-12-2019.arc-circles3': arcCircles3,

  '27-12-2019.spirograph-fast1': spiro1,
  '27-12-2019.spirograph-fast2': spiro2,
  '27-12-2019.spirograph-fast3': spiro3,
  '27-12-2019.spirograph-fast4': spiro4,
  '27-12-2019.spirograph-pretty': spiroPretty,
  '27-12-2019.spirograph5': spiro5,

  '28-12-2019.nesting1': nesting1,
  '28-12-2019.statebox3': statebox3,
  
  '31-12-2019.zoomy': zoomy,
  '31-12-2019.tunnelvision': tunnelvision,

}